@extends('base') 
@section('main')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Update a service</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br /> 
        @endif
        <form method="post" action="{{ route('services.update', $service->id) }}">
            @method('PATCH') 
            @csrf
            <div class="form-group">

                <label for="position">Position:</label>
                <input type="string" class="form-control" name="position" value={{ $service->position }} />
            </div>

            <div class="form-group">
                <label for="data">Data:</label>
                <input type="date" class="form-control" name="data" value={{ $service->data }} />
            </div>

            <div class="form-group">
                <label for="time">Time:</label>
                <input type="time" class="form-control" name="time" value={{ $service->time }} />
            </div>
            <div class="form-group">
                <label for="product">Product:</label>
                <input type="string" class="form-control" name="product" value={{ $service->product }} />
            </div>
            <div class="form-group">
                <label for="quantity">Quantity:</label>
                <input type="integer" class="form-control" name="quantity" value={{ $service->quantity }} />
            </div>
            <div class="form-group">
                <label for="vender">Vender:</label>
                <input type="string" class="form-control" name="vender" value={{ $service->vender }} />
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
@endsection