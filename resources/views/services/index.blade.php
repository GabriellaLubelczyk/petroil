@extends('base')

@section('main')
<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">Services</h1>    
  <table class="table table-striped">
  <div>
    <a style="margin: 19px;" href="{{ route('services.create')}}" class="btn btn-primary">New service</a>
    </div>  
    <thead>
        <tr>
          <td>ID</td>
          <td>Position</td>
          <td>Data</td>
          <td>Time</td>
          <td>Product</td>
          <td>Quantity</td>
          <td>Vender</td>
          <td colspan = 2>Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($services as $service)
        <tr>
            <td>{{$service->id}}</td>
            <td>{{$service->position}}</td>
            <td>{{$service->data}}</td>
            <td>{{$service->time}}</td>
            <td>{{$service->product}}</td>
            <td>{{$service->quantity}}</td>
            <td>{{$service->vender}}</td>
            <td>
                <a href="{{ route('services.edit',$service->id)}}" class="btn btn-primary">Edit</a>
            </td>
            <td>
                <form action="{{ route('services.destroy', $service->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
</div>
@endsection