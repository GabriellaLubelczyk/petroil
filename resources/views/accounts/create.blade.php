@extends('base')

@section('main')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Add account</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('accounts.store') }}">
          @csrf
          <div class="form-group">    
              <label for="name">Name:</label>
              <input type="string" class="form-control" name="name"/>
          </div>

          <div class="form-group">
              <label for="surname">Surname:</label>
              <input type="string" class="form-control" name="surname"/>
          </div>

          <div class="form-group">
              <label for="email">Email:</label>
              <input type="string" class="form-control" name="email"/>
          </div>
          <div class="form-group">
              <label for="phone_number">Phone number:</label>
              <input type="string" class="form-control" name="phone_number"/>
          </div>
          <div class="form-group">
              <label for="address">Address:</label>
              <input type="text" class="form-control" name="address"/>
          </div>
          <div class="form-group">
              <label for="city">City:</label>
              <input type="string" class="form-control" name="city"/>
          </div> 
          <div class="form-group">
              <label for="country">Country:</label>
              <input type="string" class="form-control" name="country"/>
          </div>                       
          <p> {{$errors}} </p>
          <button type="submit" class="btn btn-primary-outline">Add account</button>
      </form>
  </div>
</div>
</div>
@endsection