@extends('base') 
@section('main')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Update an account</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br /> 
        @endif
        <form method="post" action="{{ route('accounts.update', $account->id) }}">
            @method('PATCH') 
            @csrf
            <div class="form-group">

                <label for="name">Name:</label>
                <input type="string" class="form-control" name="name" value={{ $account->name }} />
            </div>

            <div class="form-group">
                <label for="surname">Surname:</label>
                <input type="string" class="form-control" name="surname" value={{ $account->surname }} />
            </div>

            <div class="form-group">
                <label for="email">Email:</label>
                <input type="string" class="form-control" name="email" value={{ $account->email }} />
            </div>

            <div class="form-group">
                <label for="phone_number">Phone Number:</label>
                <input type="string" class="form-control" name="phone_number" value={{ $account->phone_number }} />
            </div>

            <div class="form-group">
                <label for="address">Address:</label>
                <input type="text" class="form-control" name="address" value={{ $account->address }} />
            </div>
            <div class="form-group">
                <label for="city">City:</label>
                <input type="string" class="form-control" name="city" value={{ $account->city }} />
            </div>
            <div class="form-group">
                <label for="country">Country:</label>
                <input type="string" class="form-control" name="country" value={{ $account->country }} />
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
@endsection