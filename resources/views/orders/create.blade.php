@extends('base')

@section('main')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Add a orders</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('orders.store') }}">
          @csrf
          <div class="form-group">    
              <label for="dimension">Dimension:</label>
              <input type="integer" class="form-control" name="dimension"/>
          </div>

          <div class="form-group">
              <label for="type_oil">Type of oil:</label>
              <input type="string" class="form-control" name="type_oil"/>
          </div>

          <div class="form-group">
              <label for="oil_liter">Oil liter:</label>
              <input type="integer" class="form-control" name="oil_liter"/>
          </div>
          <div class="form-group">
              <label for="cost">Cost:</label>
              <input type="integer" class="form-control" name="cost"/>
          </div>
          <div class="form-group">
              <label for="delivery">Delivery:</label>
              <select name="delivery">
              <option value="true">Active</option> 
              <option value="false">Not Active</option>
              </select>
            <p> {{$orders}} </p>
          </div>
          <button type="submit" class="btn btn-primary-outline">Add contact</button>
      </form>
  </div>
</div>
</div>
@endsection