@extends('base')

@section('main')
<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">Petroils</h1>    
  <table class="table table-striped">
  <div>
    <a style="margin: 19px;" href="{{ route('petroils.create')}}" class="btn btn-primary">New Petroil</a>
    </div>  
    <thead>
        <tr>
          <td>ID</td>
          <td>Type oil</td>
          <td>Percentage</td>
          <td>Cost for hour</td>
          <td colspan = 2>Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($petroils as $petroil)
        <tr>
            <td>{{$petroil->id}}</td>
            <td>{{$petroil->type_oil}}</td>
            <td>{{$petroil->percentage}}</td>
            <td>{{$petroil->cost_hour}}</td>
            <td>
                <a href="{{ route('petroils.edit',$petroil->id)}}" class="btn btn-primary">Edit</a>
            </td>
            <td>
                <form action="{{ route('petroils.destroy', $petroil->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
</div>
@endsection