@extends('base') 
@section('main')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Update a petroil</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br /> 
        @endif
        <form method="post" action="{{ route('petroils.update', $petroil->id) }}">
            @method('PATCH') 
            @csrf
            <div class="form-group">

                <label for="type_oil">Type of oil:</label>
                <input type="string" class="form-control" name="type_oil" value={{ $petroil->type_oil }} />
            </div>

            <div class="form-group">
                <label for="percentage">Percentage:</label>
                <input type="integer" class="form-control" name="percentage" value={{ $petroil->percentage }} />
            </div>

            <div class="form-group">
                <label for="cost_hour">Cost at hour:</label>
                <input type="integer" class="form-control" name="cost_hour" value={{ $petroil->cost_hour }} />
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
@endsection