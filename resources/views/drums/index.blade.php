@extends('base')

@section('main')
<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">Drums</h1>    
  <table class="table table-striped">
  <div>
    <a style="margin: 19px;" href="{{ route('drums.create')}}" class="btn btn-primary">New drum</a>
    </div>  
    <thead>
        <tr>
          <td>ID</td>
          <td>Dimension</td>
          <td>Max liter</td>
          <td>Position</td>
          <td colspan = 2>Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($drums as $drums)
        <tr>
            <td>{{$drums->id}}</td>
            <td>{{$drums->dimension}}</td>
            <td>{{$drums->max_liter}}</td>
            <td>{{$drums->position}}</td>
            <td>
                <a href="{{ route('drums.edit',$drums->id)}}" class="btn btn-primary">Edit</a>
            </td>
            <td>
                <form action="{{ route('drums.destroy', $drums->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
</div>
@endsection