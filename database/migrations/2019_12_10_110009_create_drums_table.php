<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDrumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drums', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->integer('dimension')->nullable();
            $table->integer('max_liter');
            $table->string('position');               
            $table->integer('order_id')->nullable();
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');   
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drums');
    }
}
