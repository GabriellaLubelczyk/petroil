<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        DB::table('accounts')->insert([
            'name'=>'Luca',
            'surname'=>'Colla',
            'email'=>'luca.colla@gmail.com',
            'phone_number' =>'3291820482',
            'address'=>'Via Gigi 88',
            'city'=>'Firenze',
            'country'=>'Italia'
         ]);
         DB::table('accounts')->insert([
            'name'=>'Vittorio',
            'surname'=>'Andrei',
            'email'=>'vitto.andreis@gmail.com',
            'phone_number' =>'1111111111',
            'address'=>'Viale fame 11',
            'city'=>'Roma',
            'country'=>'Italia'
         ]);
         DB::table('accounts')->insert([
            'name'=>'Mario',
            'surname'=>'Rossi',
            'email'=>'mario.rossi@gmail.com',
            'phone_number' =>'3298194839',
            'address'=>'Via dei sognatori 18',
            'city'=>'Treviso',
            'country'=>'Italia'
         ]);


         DB::table('deliveries')->insert([
            'name_customer'=>'Mario',
            'dimension'=>'200',
            'type_oil'=>'WTI',
            'oil_liter' =>'666',
            'cost'=>'500',
            'address'=>'Via dei sognatori 18',
            'phone_number'=>'3298194839'
         ]);
         DB::table('deliveries')->insert([
            'name_customer'=>'Luca',
            'dimension'=>'350',
            'type_oil'=>'Brent',
            'oil_liter' =>'450',
            'cost'=>'780',
            'address'=>'Via Gigi 88',
            'phone_number'=>'3291820482'
         ]);
         DB::table('deliveries')->insert([
            'name_customer'=>'Mario',
            'dimension'=>'180',
            'type_oil'=>'Brent',
            'oil_liter' =>'888',
            'cost'=>'360',
            'address'=>'Via dei sognatori 18',
            'phone_number'=>'3298194839'
         ]);
         DB::table('deliveries')->insert([
            'name_customer'=>'Vittorio',
            'dimension'=>'580',
            'type_oil'=>'Brent',
            'oil_liter' =>'1000',
            'cost'=>'485',
            'address'=>'Viale fame 11',
            'phone_number'=>'1111111111'
         ]);


         DB::table('drums')->insert([
            'dimension'=>'180',
            'max_liter'=>'900',
            'position'=>'Treviso',
            'order_id'=>'1'
         ]);
         DB::table('drums')->insert([
            'dimension'=>'580',
            'max_liter'=>'1000',
            'position'=>'Roma',
            'order_id'=>'1'
         ]);
         DB::table('drums')->insert([
            'dimension'=>'200',
            'max_liter'=>'700',
            'position'=>'Treviso',
            'order_id'=>'3'
         ]);

         DB::table('drums')->insert([
            'dimension'=>'200',
            'max_liter'=>'700',
            'position'=>'Firenze',
            'order_id'=>'2'
         ]);
         DB::table('drums')->insert([
            'dimension'=>'350',
            'max_liter'=>'850',
            'position'=>'Treviso',
            'order_id'=>'1'
         ]);


         DB::table('orders')->insert([
            'dimension'=>'350',
            'type_oil'=>'Brent',
            'oil_liter'=>'850',
            'cost'=>'780',
            'delivery'=>true,
            'account_id' => '2'
         ]);
         DB::table('orders')->insert([
            'dimension'=>'180',
            'type_oil'=>'Brent',
            'oil_liter'=>'300',
            'cost'=>'150',
            'delivery'=>true,
            'account_id' => '1'
         ]);

        DB::table('orders')->insert([
            'dimension'=>'400',
            'type_oil'=>'WTI',
            'oil_liter'=>'300',
            'cost'=>'150',
            'delivery'=>true,
            'account_id' => '3'
        ]);
        DB::table('petroils')->insert([
            'type_oil'=>'Brent',
            'percentage'=>'66',
            'cost_hour'=>'20'
        ]);

        DB::table('petroils')->insert([
            'type_oil'=>'WTI',
            'percentage'=>'78',
            'cost_hour'=>'50'
        ]);
        DB::table('petroils')->insert([
            'type_oil'=>'Brent',
            'percentage'=>'33',
            'cost_hour'=>'15'
        ]);
        DB::table('petroils')->insert([
            'type_oil'=>'WTI',
            'percentage'=>'45',
            'cost_hour'=>'65'
        ]);

        DB::table('services')->insert([
            'position'=>'Roma',
            'data'=>'18/08/2020',
            'time'=>'13:00',
            'product'=>'WTI',
            'quantity'=>'1',
            'vender'=>'Caio'
        ]);
        DB::table('services')->insert([
            'position'=>'Torino',
            'data'=>'28/03/2019',
            'time'=>'18:00',
            'product'=>'Brent',
            'quantity'=>'31',
            'vender'=>'Tizio'
        ]);
        DB::table('services')->insert([
            'position'=>'Firenze',
            'data'=>'24/12/2019',
            'time'=>'17:15',
            'product'=>'WTI',
            'quantity'=>'2',
            'vender'=>'Luigi'
        ]);
}
    }

