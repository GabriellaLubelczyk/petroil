<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;
use App\Order;
use Log;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Log::info('Account index');

        $accounts = Account::all();

        return view('accounts.index', compact('accounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Log::info('Account create');
        $accounts = Account::find(3)->orders;
        Log::info($accounts);

        return view('accounts.create');
 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log::info('Account store');

        $request->validate([
            'name'=>'required',
            'surname'=>'required',
            'email'=>'required'
        ]);

        $account = new Account([
            'name' => $request->get('name'),
            'surname' => $request->get('surname'),
            'email' => $request->get('email'),
            'phone_number' => $request->get('phone_number'),
            'address' => $request->get('address'),
            'city' => $request->get('city'),
            'country' => $request->get('country')
        ]);
        $account->save();
        return redirect('/accounts')->with('success', 'Account saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Log::info('Account edit');

        $account = Account::find($id);
        return view('accounts.edit', compact('account'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Log::info('Account update');

        $request->validate([
            'name'=>'required',
            'surname'=>'required',
            'email'=>'required'
        ]);

        $account = Account::find($id);
        $account->name =  $request->get('name');
        $account->surname = $request->get('surname');
        $account->email = $request->get('email');
        $account->phone_number = $request->get('phone_number');
        $account->address = $request->get('address');
        $account->city = $request->get('city');
        $account->country = $request->get('country');
        $account->save();

        return redirect('/accounts')->with('success', 'Account updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $account = Account::find($id);
        $account->delete();

        return redirect('/accounts')->with('success', 'Account deleted!');
    }
}
