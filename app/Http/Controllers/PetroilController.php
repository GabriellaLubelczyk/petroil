<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Petroil;
use Log;
class PetroilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Log::info('Petroil index');

        $petroils = Petroil::all();

        return view('petroils.index', compact('petroils'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Log::info('Petroil Create');

        return view('petroils.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log::info('Petroil Store');

        $request->validate([
            'type_oil'=>'required',
            'percentage'=>'required',
            'cost_hour'=>'required'
        ]);

        $petroil = new Petroil([
            'type_oil' => $request->get('type_oil'),
            'percentage' => $request->get('percentage'),
            'cost_hour' => $request->get('cost_hour')
        ]);
        $petroil->save();
        return redirect('/petroils')->with('success', 'Petroil saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        Log::info('Petroil edit');

        $petroil = Petroil::find($id);
        return view('petroils.edit', compact('petroil')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        Log::info('Petroil update');

        $request->validate([
            'type_oil'=>'required',
            'percentage'=>'required',
            'cost_hour'=>'required'
        ]);

        $petroil = Petroil::find($id);
        $petroil->type_oil =  $request->get('type_oil');
        $petroil->percentage = $request->get('percentage');
        $petroil->cost_hour = $request->get('cost_hour');
        $petroil->save();

        return redirect('/petroils')->with('success', 'Petroil updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Log::info('Petroil destroy');

        $petroil = Petroil::find($id);
        $petroil->delete();

        return redirect('/petroils')->with('success', 'Petroil deleted!');
    }
}
