<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Delivery;
use Log;

class DeliveryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Log::info('Delivery index');

        $deliveries = Delivery::all();

        return view('deliveries.index', compact('deliveries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Log::info('Delivery create');

        return view('deliveries.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log::info('Delivery store');

        $request->validate([
            'name_customer'=>'required',
            'dimension'=>'required',
            'type_oil'=>'required',
            'oil_liter'=>'required',
            'cost'=>'required',
            'address'=>'required',
            'phone_number'=>'required'
        ]);

        $delivery = new Delivery([
            'name_customer' => $request->get('name_customer'),
            'dimension' => $request->get('dimension'),
            'type_oil' => $request->get('type_oil'),
            'oil_liter' => $request->get('oil_liter'),
            'cost' => $request->get('cost'),
            'address' => $request->get('address'),
            'phone_number' => $request->get('phone_number')
        ]);
        $delivery->save();
        return redirect('/deliveries')->with('success', 'Delivery saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Log::info('Delivery edit');

        $delivery = Delivery::find($id);
        return view('deliveries.edit', compact('delivery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Log::info('Delivery update');

        $request->validate([
            'name_customer'=>'required',
            'dimension'=>'required',
            'type_oil'=>'required',
            'oil_liter'=>'required',
            'cost'=>'required',
            'address'=>'required',
            'phone_number'=>'required'
        ]);

        $delivery = Delivery::find($id);
        $delivery->name_customer =  $request->get('name_customer');
        $delivery->dimension = $request->get('dimension');
        $delivery->type_oil = $request->get('type_oil');
        $delivery->oil_liter = $request->get('oil_liter');
        $delivery->cost = $request->get('cost');
        $delivery->address = $request->get('address');
        $delivery->phone_number = $request->get('phone_number');
        $delivery->save();

        return redirect('/deliveries')->with('success', 'Delivery updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Log::info('Delivery destroy');

        $delivery = Delivery::find($id);
        $delivery->delete();

        return redirect('/deliveries')->with('success', 'Delivery deleted!');
    }
}
