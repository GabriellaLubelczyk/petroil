<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use Log;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Log::info('Service index');

        $services = Service::all();

        return view('services.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Log::info('Service create');

        return view('services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log::info('Service store');

        $request->validate([
            'position'=>'required',
            'data'=>'required',
            'time'=>'required',
            'product'=>'required',
            'quantity'=>'required',
            'vender'=>'required'
        ]);

        $service = new Service([
            'position' => $request->get('position'),
            'data' => $request->get('data'),
            'time' => $request->get('time'),
            'product' => $request->get('product'),
            'quantity' => $request->get('quantity'),
            'vender' => $request->get('vender')
        ]);
        $service->save();
        return redirect('/services')->with('success', 'Service saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Log::info('Service edit');

        $service = Service::find($id);
        return view('services.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'position'=>'required',
            'data'=>'required',
            'time'=>'required',
            'product'=>'required',
            'quantity'=>'required',
            'vender'=>'required'
        ]);

        $service = Service::find($id);
        $service->position =  $request->get('position');
        $service->data = $request->get('data');
        $service->time = $request->get('time');
        $service->product = $request->get('product');
        $service->quantity = $request->get('quantity');
        $service->vender = $request->get('vender');
        $service->save();

        return redirect('/services')->with('success', 'Service updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Log::info('Service destroy');

        $service = Service::find($id);
        $service->delete();

        return redirect('/services')->with('success', 'Service deleted!');
    }
}
