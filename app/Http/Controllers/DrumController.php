<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Drum;
use App\Order;
use Log;

class DrumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Log::info('Drum index');

        $drums = Drum::all();
        return view('drums.index', compact('drums'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Log::info('Drum create');

        //order
        $drums = Drum::find(3)->orders;
        Log::info($drums);
        return view('drums.create', compact('orders'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log::info('Drum store');

        /*$request->validate([
            'dimension'=>'required',
            'max_liter'=>'required',
            'position'=>'required'
        ]);
*/
        $drum = new Drum([
            'dimension' => $request->get('dimension'),
            'max_liter' => $request->get('max_liter'),
            'position' => $request->get('position')
        ]);
        $drum->save();
        return redirect('/drums')->with('success', 'Drum saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Log::info('Drum edit');
        
        $drum = Drum::find($id);
        return view('drums.edit', compact('drum')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Log::info('Drum update');

        $request->validate([
            'dimension'=>'required',
            'max_liter'=>'required',
            'position'=>'required'
        ]);

        $drum = Drum::find($id);
        $drum->dimension =  $request->get('dimension');
        $drum->max_liter = $request->get('max_liter');
        $drum->position = $request->get('position');
        $drum->save();

        return redirect('/drums')->with('success', 'Drum updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Log::info('Drum destroy');

        $drum = Drum::find($id);
        $drum->delete();

        return redirect('/drums')->with('success', 'Drum deleted!');
    }
}
