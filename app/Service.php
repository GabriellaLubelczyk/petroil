<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        'position',
        'data',
        'time',
        'product',
        'quantity',
        'vender'       
    ];
}
