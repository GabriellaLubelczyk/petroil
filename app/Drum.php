<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Drum extends Model
{
    protected $fillable = [
        'dimesion',
        'max_liter',
        'position'     
    ];
    
    //orders
    public function orders(){

        return $this->belongsTo(Order::class);
        
        } 
}
