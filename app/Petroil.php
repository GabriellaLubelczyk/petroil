<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Petroil extends Model
{
    protected $fillable = [
        'type_oil',
        'percentage',
        'cost_hour'      
    ];
}
