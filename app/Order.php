<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Account;
use App\Drum;

class Order extends Model
{
    protected $fillable = [
        'dimension',
        'type_oil',
        'oil_liter',
        'cost',
        'delivery'
    ];
    public function account(){

        return $this->belongsTo(Account::class);
        
        }
        //drum
    public function drums(){

        return $this->hasMany(Drum::class);
            
        }
}
