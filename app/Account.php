<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;

class Account extends Model
{
    protected $fillable = [
        'name',
        'surname',
        'email',
        'phone_number',
        'address',
        'city',
        'country'     
    ];

    public function orders(){

        return $this->hasMany(Order::class);
        
        }
}
